/*
 * 	Application01.cpp
 *
 *  Created on: 27.12.2018
 *      Author: Jarkko
 */

#include<iostream>
#include<fstream> // filestream library
#include<iomanip>

using namespace std;

int main(){

	cout << left;
	string employee_name;
	int employee_salary;

	// Filestream
	ifstream employee_file("Employees.txt");
	if(!employee_file){
		cout <<"Employee text file not found.\n"<<endl;
		return -7;
	}

	cout << "\t\t\tHUMAN RECOURCES PAYROLL LIST\n\n";
	cout << setw(35) << "FULL NAME"<< "SALARY" <<endl <<endl;

	// Ignore employees.txt file two first line
	employee_file.ignore(255,'\n');
	employee_file.ignore(255,'\n');

	// parsing emplyee names.Delimiter is comma
	getline(employee_file, employee_name, ',');
	employee_file >> employee_salary;

	while(!employee_file.eof()){
		cout<< setw(35) << employee_name << employee_salary <<endl;
		getline(employee_file,employee_name,',');
		employee_file >> employee_salary;
	}

	return 0;
}
