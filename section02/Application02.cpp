/*
 * 	Application02.cpp
 *
 *  Created on: 27.12.2018
 *      Author: Jarkko
 */

#include<iostream>
#include<fstream> // filestream library
#include<iomanip>

using namespace std;


void print_hello(){
	cout<<"HELLO!" <<endl;
}

double get_age(){
	double age = 23.0;
	return age;
}


string get_name(){
	return "String";
}

int main(){
	print_hello();
	cout << get_name() << endl;
	cout << get_age();
	return 0;
}
