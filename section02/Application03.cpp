/*
 * 	Application03.cpp
 *
 *  Created on: 27.12.2018
 *      Author: Jarkko
 */

#include<iostream>


using namespace std;

string print_name(string name){
	return name;
}

int get_age(double x){
	return x;
}

int main(){
	string name;
	double age;
	cout <<"Enter a name:";
	cin >> name;
	cout<<"\nEnter age: ";
	cin >> age;
	cout<< print_name(name);
	cout<< get_age(age);
	return 0;
}
