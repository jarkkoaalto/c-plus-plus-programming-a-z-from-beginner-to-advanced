/*
 * 	Arrays.cpp
 *
 *  Created on: 17.12.2018
 *      Author: Jarkko
 */

#include<iostream>
#include<fstream>

using namespace std;

int main(){
	const int SIZE = 7;
	string grocery_list[SIZE] = {"Eggs","Milk","Bread","Banana","Coffee","Apple","Chicken wing"};
	for(int i = 0; i<SIZE;i++){
		cout << grocery_list[i] <<endl;
	}



	ofstream output_file("names.txt");
	if(!output_file){
		cout << "The file could not be found."<<endl;
		return -5;
	}
	string name ="Hi there!";
	output_file << name;
	return 0;
}
