/*
 * Basics.cpp
 *
 *  Created on: 17.12.2018
 *      Author: Jarkko
 */

#include <iostream>

using namespace std;

int main(){

	char letter = 'z';
	cout<<letter<<endl <<letter<<endl;

	double box2 = 89.34;
	cout << box2 << endl;

	string address = "10000 Collage Street";
	cout << endl << address <<endl;

	string name = "Joe";
	cout << name;
	cout<<endl<<"Is teaching in class"<<endl;

	string name1 = "Zach";
	int age = 23;
	cout<<name<< " is "<<age<< " years old."<<endl;

	string name_2;
	cout<<"Enter name : "<<endl;
	cin>>name_2;
	int age_2;
	cout<<"Enter your age:"<<endl;
	cin>>age_2;
	cout<<"Your name is "<< name_2<<" and you are "<<age_2<<" years old";

	return 0;
}
