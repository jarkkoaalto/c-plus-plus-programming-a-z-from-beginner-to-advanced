/*
 * 	FunctionOverloading.cpp
 *
 *  Created on: 27.12.2018
 *      Author: Jarkko
 */

#include<iostream>

using namespace std;

void print_salary(int);
void print_salary(double);
void print_salary(string);

int main(){
	// int salary;
	 string salary;
	//double salary;

	cout <<"Enter your salary :";
	cin >> salary;
	print_salary(salary);

	return 0;
}


void print_salary(int x){
	cout << endl<< x <<endl;
}
void print_salary(double x){
	cout << endl<< x <<endl;
}

void print_salary(string x){
	cout << endl<< x <<endl;
}


