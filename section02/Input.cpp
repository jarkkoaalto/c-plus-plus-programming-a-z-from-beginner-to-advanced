/*
 * 	Input.cpp
 *
 *  Created on: 17.12.2018
 *      Author: Jarkko
 */

#include<iostream>
#include<fstream>

using namespace std;

int main(){

	const int SIZE = 11;
	string names_list[SIZE];
	string temp;

	ifstream input_file("names.txt");
	if(!input_file){
		cout << "File not found." << endl;
	    return -6;
	}

	input_file.ignore(255,'\n');

	int index = 0;
	input_file >> temp;

	getline(input_file,temp);
	while(!input_file.eof()){
		names_list[index] = temp;
		index++;
		getline(input_file,temp);
	}

	for(int i=0; i<SIZE;i++){
		cout<<names_list[i]<<endl;
	}

	cout<<endl<<endl <<"This is a new line character:"<<endl<<endl;

	return 0;
}
