/*
 * Loops.cpp
 *
 *  Created on: 17.12.2018
 *      Author: Jarkko
 */

#include<iostream>

using namespace std;

int main(){

	int run = 10;

	// While loop
	while(run >= 0){
		cout << run;
		run = run -1;
	}

	do {
		cout<<endl<<run<<endl;
		run--;
	}while(run >= 0);


	for(int index =0; index<10; index++){
		cout<<index;
	}

	for(int i = 0; i<10;i++){
		cout<<endl<<i;
	}


	return 0;
}
