/*
 * 	Reference.cpp
 *
 *  Created on: 27.12.2018
 *      Author: Jarkko
 */

#include<iostream>


using namespace std;
void print_age(int &); // Prototuype
void change_address(string &); // Prototype

int main(){
	int age = 7;
	print_age(age); // prototype, rewrite memory address
	cout << endl <<age; // function, change memory and copy 5

	string my_address ="24455 Math Drive";
	cout<<"Address before function call: " << my_address <<endl;
	cout<<"Address after function call: "<<my_address<<endl;



	return 0;
}

void change_address(string &address){
	address = "1400 Collage drive.";

}

void print_age(int x){
	x = 5;
	cout << x;
}
