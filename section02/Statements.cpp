/*
 * Statements.cpp
 *
 *  Created on: 18.12.2018
 *      Author: Jarkko
 */

#include <iostream>
using namespace std;

int main(){

	int num1 = 0;
	cout<<"Enter (int) number: ";
	cin>>num1;

	if(num1 < 10){
		cout<<"You enter number is less than 10."<<endl;
	}
	else if(num1 == 10){
		cout<<"You enter number is 10"<<endl;
	}
	else{
		cout<<"You enter number is greater than 10."<<endl;
	}


	char grade;
	cout<<"Enter a grade:"<<endl;
	cin>>grade;

	switch(grade){
		case 'A':{
			cout<<"You made a 90 or above."<<endl;
			break;
		}
		case 'B':{
			cout<<"You made a 80 or above"<<endl;
			break;
		}
		case 'C':{
			cout<<"You made a 70 or above"<<endl;
			break;
		}
		case 'F':{
			cout<<"You failed."<<endl;
			break;
		}
		default:
			cout<<"You entered invalid value"<<endl;
	}


	return 0;
}
