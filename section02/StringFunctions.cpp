/*
 * 	StringFunctions.cpp
 *
 *  Created on: 27.12.2018
 *      Author: Jarkko
 */

#include<iostream>

using namespace std;

int main(){
	string name = "Hi There";
	cout << name.length() <<endl;
	cout << name.find('r')<< endl;
	cout << name.find('W')<<endl;

	return 0;
}
